# io.earcam.instrumental [![Build Status](https://travis-ci.org/earcam/io.earcam.instrumental.svg?branch=master)](https://travis-ci.org/earcam/io.earcam.instrumental) [![Maven Central](https://maven-badges.herokuapp.com/maven-central/io.earcam/io.earcam.instrumental/badge.svg)](https://maven-badges.herokuapp.com/maven-central/io.earcam/io.earcam.instrumental) ![Sonatype Nexus (Snapshots)](https://img.shields.io/nexus/s/https/oss.sonatype.org/io.earcam/io.earcam.instrumental.svg)


Some useful bits 'n' bobs, see [https://instrumental.earcam.io](https://instrumental.earcam.io)
